package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NonNull
    @Override
    public Project create(@NonNull final String userId, @NonNull final String name) {
        @NonNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return project;
    }

    @NonNull
    @Override
    public Project create(
            @NonNull final String userId,
            @NonNull final String name,
            @NonNull final String description
    ) {
        @NonNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(userId, project);
    }

    @Override
    public boolean existsById(@NonNull final String userId, @NonNull final String id) {
        return findOneById(userId, id) != null;
    }

}
