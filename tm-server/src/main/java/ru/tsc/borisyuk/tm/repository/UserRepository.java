package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(@NonNull final String id) {
        return items.stream().filter(uzer -> id.equals(uzer.getId()))
                .findAny().orElse(null);
    }

    @Override
    public User findByLogin(@NonNull final String login) {
        return items.stream().filter(uzer -> login.equals(uzer.getLogin()))
                .findAny().orElse(null);
    }

    @Override
    public User findByEmail(@NonNull final String email) {
        return items.stream().filter(uzer -> email.equals(uzer.getEmail()))
                .findAny().orElse(null);
    }

    @Override
    public boolean existsByLogin(@NonNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean existsByEmail(@NonNull final String email) {
        return findByEmail(email) != null;
    }

}
