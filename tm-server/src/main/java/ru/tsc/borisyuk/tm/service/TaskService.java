package ru.tsc.borisyuk.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.exception.field.DescriptionEmptyException;
import ru.tsc.borisyuk.tm.exception.field.NameEmptyException;
import ru.tsc.borisyuk.tm.exception.field.TaskIdEmptyException;
import ru.tsc.borisyuk.tm.exception.field.UserIdEmptyException;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        final Task task = repository.create(userId, name, description);
        if (task == null) throw new TaskNotFoundException();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new TaskIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new TaskIdEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
