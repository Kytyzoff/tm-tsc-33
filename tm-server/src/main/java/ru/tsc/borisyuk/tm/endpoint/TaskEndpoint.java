package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.borisyuk.tm.api.service.IProjectTaskService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    private IProjectTaskService projectTaskService = getServiceLocator().getProjectTaskService();

    @NonNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskChangeStatusByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Task task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Task task = getTaskService().changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskRemoveByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskRemoveByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskGetByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskGetByIndexResponse getTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskGetByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskUpdateByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskUpdateByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskCreateRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskListRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Sort sort = request.getSort();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskClearRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskBindToProjectRequest request) {
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String projectId = request.getProjectId();
        final String taskId = request.getTaskId();
        final Task task = projectTaskService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NonNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final TaskUnbindFromProjectRequest request) {
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String projectId = request.getProjectId();
        final String taskId = request.getTaskId();
        final Task task = projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

}
