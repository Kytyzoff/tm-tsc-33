package ru.tsc.borisyuk.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.service.IAuthService;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.exception.field.LoginEmptyException;
import ru.tsc.borisyuk.tm.exception.field.PasswordEmptyException;
import ru.tsc.borisyuk.tm.exception.system.AccessDeniedException;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.CryptUtil;
import ru.tsc.borisyuk.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NonNull
    private final IUserService userService;

    @NonNull
    private final IPropertyService propertyService;

    public AuthService(
            @NonNull final IPropertyService propertyService,
            @NonNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public User register(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User check(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = userService.findOneByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(propertyService, password)))
            throw new AccessDeniedException();
        return user;
    }

    @NonNull
    @Override
    @SneakyThrows
    public String login(final String login, final String password) {
        if (login == null) throw new LoginEmptyException();
        if (password == null) throw new PasswordEmptyException();
        final User user = userService.findOneByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NonNull
    private String getToken(final User user) {
        return getToken(createSession(user));
    }

    private Session createSession(@NonNull final User user) {
        @NonNull Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        return session;
    }

    @NonNull
    @SneakyThrows
    private String getToken(final Session session) {
        @NonNull final ObjectMapper objectMapper = new ObjectMapper();
        @NonNull final String token = objectMapper.writeValueAsString(session);
        @NonNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NonNull
    @Override
    @SneakyThrows
    public Session validateToken(final String token) {
        if (token == null) throw new AccessDeniedException();
        @NonNull final String sessionKey = propertyService.getSessionKey();
        @NonNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NonNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NonNull final ObjectMapper objectMapper = new ObjectMapper();
        @NonNull Session session = objectMapper.readValue(json, Session.class);
        @NonNull final Date currentDate = new Date();
        @NonNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NonNull final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

}
