package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IRepository;
import ru.tsc.borisyuk.tm.api.service.IService;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.exception.entity.ItemNotFoundException;
import ru.tsc.borisyuk.tm.exception.field.CollectionEmptyException;
import ru.tsc.borisyuk.tm.exception.field.IdEmptyException;
import ru.tsc.borisyuk.tm.exception.field.IndexIncorrectException;
import ru.tsc.borisyuk.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NonNull
    protected final R repository;

    public AbstractService(@NonNull final R repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NonNull
    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    public M add(final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.add(item);
    }

    @Override
    public Collection<M> addAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) throw new CollectionEmptyException();
        return repository.addAll(collection);
    }

    @Override
    public M remove(final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.remove(item);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) throw new CollectionEmptyException();
        repository.removeAll(collection);
    }

    @Override
    public M removeById(final String id) {
        final M item = findOneById(id);
        if (item == null) throw new ItemNotFoundException();
        return remove(item);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M item = findOneByIndex(index);
        if (item == null) throw new ItemNotFoundException();
        return remove(item);
    }

    @Override
    public Collection<M> set(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) throw new CollectionEmptyException();
        return repository.set(collection);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.size()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public int size() {
        return repository.size();
    }

}
