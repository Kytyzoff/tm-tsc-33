package ru.tsc.borisyuk.tm.util;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NonNull
    String PATTERN = "dd.MM.yyyy";

    @NonNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String value) {
        if (StringUtils.isEmpty(value)) return null;
        try {
            return FORMATTER.parse(value);
        } catch (@NonNull final ParseException e) {
            System.err.println("ParseException : " + e.getLocalizedMessage());
            return null;
        }
    }

    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
