package ru.tsc.borisyuk.tm;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        @NonNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
