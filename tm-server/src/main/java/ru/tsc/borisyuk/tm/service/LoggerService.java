package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.service.ILoggerService;
import ru.tsc.borisyuk.tm.exception.system.LogDirectoryCreateException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    @NonNull
    private static final String LOG_DIRECTORY = "log";

    @NonNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NonNull
    private static final String COMMANDS = "COMMANDS";

    @NonNull
    private static final String COMMANDS_FILE = Paths.get(LOG_DIRECTORY, "commands.xml").toString();

    @NonNull
    private static final String ERRORS = "ERRORS";

    @NonNull
    private static final String ERRORS_FILE = Paths.get(LOG_DIRECTORY, "errors.xml").toString();

    @NonNull
    private static final String MESSAGES = "MESSAGES";

    @NonNull
    private static final String MESSAGES_FILE = Paths.get(LOG_DIRECTORY, "messages.xml").toString();

    @NonNull
    private final LogManager logManager = LogManager.getLogManager();

    @NonNull
    private final Logger rootLogger = Logger.getLogger("");

    @NonNull
    private final Logger commandsLogger = Logger.getLogger(COMMANDS);

    @NonNull
    private final Logger errorsLogger = Logger.getLogger(ERRORS);

    @NonNull
    private final Logger messagesLogger = Logger.getLogger(MESSAGES);

    @NonNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        checkLogDirectory();
        init();
        register(commandsLogger, COMMANDS_FILE, false);
        register(errorsLogger, ERRORS_FILE, true);
        register(messagesLogger, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            logManager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (@NonNull final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private void checkLogDirectory() {
        @NonNull final Path logsDirectoryPath = Paths.get(LOG_DIRECTORY);
        if (Files.exists(logsDirectoryPath)) {
            if (!Files.isDirectory(logsDirectoryPath) || !Files.isWritable(logsDirectoryPath)) {
                throw new LogDirectoryCreateException(logsDirectoryPath.toAbsolutePath().toString());
            }
        } else {
            try {
                Files.createDirectory(logsDirectoryPath);
            } catch (@NonNull final IOException e) {
                rootLogger.severe(e.getMessage());
            }
        }
    }

    private void register(@NonNull final Logger logger, @NonNull final String logFileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(logFileName));
        } catch (@NonNull final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        @NonNull final ConsoleHandler consoleHandler = new ConsoleHandler();
        @NonNull final Formatter formatter = new Formatter() {
            @Override
            public String format(final LogRecord record) {
                return record.getMessage() + "\n";
            }
        };
        consoleHandler.setFormatter(formatter);
        return consoleHandler;
    }

    @Override
    public void info(final String message) {
        if (StringUtils.isEmpty(message)) return;
        messagesLogger.info(message);
    }

    @Override
    public void debug(final String message) {
        if (StringUtils.isEmpty(message)) return;
        messagesLogger.fine(message);
    }

    @Override
    public void command(final String message) {
        if (StringUtils.isEmpty(message)) return;
        commandsLogger.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errorsLogger.log(Level.SEVERE, e.getMessage(), e);
    }

}
