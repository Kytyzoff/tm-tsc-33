package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IUserEndpoint;
import ru.tsc.borisyuk.tm.api.service.IAuthService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.IUserEndpoint")
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NonNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        final String login = request.getLogin();
        final User user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @NonNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        final String login = request.getLogin();
        final User user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @NonNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserChangePasswordRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String password = request.getPassword();
        final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NonNull
    @Override
    @WebMethod
    public UserRegisterResponse registerUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserRegisterRequest request) {
        final String login = request.getLogin();
        final String password = request.getPassword();
        final String email = request.getEmail();
        @NonNull final IAuthService authService = getServiceLocator().getAuthService();
        final User user = authService.register(login, password, email);
        return new UserRegisterResponse(user);
    }

    @NonNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        final String login = request.getLogin();
        final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NonNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserUpdateProfileRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String firstName = request.getFirstName();
        final String lastName = request.getLastName();
        final String middleName = request.getMiddleName();
        final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }
}