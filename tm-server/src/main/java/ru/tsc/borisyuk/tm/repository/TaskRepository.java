package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NonNull
    @Override
    public Task create(@NonNull final String userId, @NonNull final String name) {
        @NonNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return task;
    }

    @NonNull
    @Override
    public Task create(@NonNull final String userId, @NonNull final String name, @NonNull final String description) {
        @NonNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NonNull
    @Override
    public List<Task> findAllByProjectId(@NonNull final String userId, @NonNull final String projectId) {
        return items.stream()
                .filter(task -> userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@NonNull final String userId, @NonNull final String id) {
        return findOneById(userId, id) != null;
    }

}
