package ru.tsc.borisyuk.tm.api.service;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFaster();

    void saveDataJsonFaster();

    void loadDataJsonJaxb();

    void saveDataJsonJaxb();

    void loadDataXmlFaster();

    void saveDataXmlFaster();

    void loadDataXmlJaxb();

    void saveDataXmlJaxb();

    void loadDataYamlFaster();

    void saveDataYamlFaster();

}
