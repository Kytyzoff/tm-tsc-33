package ru.tsc.borisyuk.tm.util;

import lombok.NonNull;
import lombok.SneakyThrows;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public interface CryptUtil {

    String TYPE = "AES/ECB/PKCS5Padding";

    String CHARSET = "UTF-8";

    @NonNull
    @SneakyThrows
    static SecretKeySpec getKey(@NonNull final String secretKey) {
        @NonNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
        @NonNull final byte[] key = secretKey.getBytes(CHARSET);
        @NonNull final byte[] digest = sha.digest(key);
        @NonNull final byte[] secret = Arrays.copyOf(digest, 16);
        return new SecretKeySpec(secret, "AES");
    }

    @NonNull
    @SneakyThrows
    static String encrypt(@NonNull final String secret, @NonNull final String strToEncrypt) {
        @NonNull final SecretKeySpec secretKey = getKey(secret);
        @NonNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NonNull final byte[] bytes = strToEncrypt.getBytes(CHARSET);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @NonNull
    @SneakyThrows
    static String decrypt(@NonNull final String secret, @NonNull final String strToDecrypt) {
        @NonNull final SecretKeySpec secretKey = getKey(secret);
        @NonNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
    }

}

