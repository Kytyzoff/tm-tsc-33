package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProject(String userId, String projectId);

    void removeProject(String userId, Project project);

}
