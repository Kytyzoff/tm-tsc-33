package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.User;

public interface IAuthService {

    User register(String login, String password, String email);

    User check(String login, String password);

    @NonNull
    String login(String login, String password);

    @NonNull
    Session validateToken(String token);

}
