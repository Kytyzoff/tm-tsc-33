package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.IDomainEndpoint")
@NoArgsConstructor
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(final @NonNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataJsonFasterLoadResponse loadDataJsonFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataJsonFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFaster();
        return new DataJsonFasterLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataJsonJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataXmlFasterLoadResponse loadDataXmlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataXmlFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFaster();
        return new DataXmlFasterLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataXmlJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataYamlFasterLoadResponse loadDataYamlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataYamlFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFaster();
        return new DataYamlFasterLoadResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataJsonFasterSaveResponse saveDataJsonFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataJsonFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFaster();
        return new DataJsonFasterSaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataJsonJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataXmlFasterSaveResponse saveDataXmlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataXmlFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFaster();
        return new DataXmlFasterSaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataXmlJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NonNull
    @Override
    @WebMethod
    public DataYamlFasterSaveResponse saveDataYamlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final DataYamlFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFaster();
        return new DataYamlFasterSaveResponse();
    }

}
