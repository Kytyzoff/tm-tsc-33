package ru.tsc.borisyuk.tm.component;

import lombok.NonNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NonNull
    private final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @NonNull
    private final Bootstrap bootstrap;

    public Backup(@NonNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        scheduledExecutorService.scheduleWithFixedDelay(this::save, 0, 30, TimeUnit.SECONDS);
    }

    public void stop() {
        scheduledExecutorService.shutdown();
    }

    private void load() {
        final boolean backupExists = Files.exists(Paths.get(bootstrap.getPropertyService().getDataFileBackupXml()));
        if (backupExists) bootstrap.getDomainService().saveDataBackup();
    }

    private void save() {
        bootstrap.getDomainService().loadDataBackup();
    }

}
