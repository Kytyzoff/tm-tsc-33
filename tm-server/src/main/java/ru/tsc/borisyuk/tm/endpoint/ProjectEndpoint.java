package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectRemoveByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectRemoveByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectGetByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectGetByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectUpdateByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectUpdateByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectCreateRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectListRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Sort sort = request.getSort();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NonNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectClearRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

}
