package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.borisyuk.tm.api.service.IAuthService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLogoutRequest;
import ru.tsc.borisyuk.tm.dto.request.UserProfileRequest;
import ru.tsc.borisyuk.tm.dto.response.UserLoginResponse;
import ru.tsc.borisyuk.tm.dto.response.UserLogoutResponse;
import ru.tsc.borisyuk.tm.dto.response.UserProfileResponse;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.IAuthEndpoint")
@NoArgsConstructor
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    @SneakyThrows
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserLoginRequest request) {
        @NonNull final IAuthService authService = getServiceLocator().getAuthService();
        @NonNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NonNull
    @Override
    @SneakyThrows
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserLogoutRequest request) {
        check(request);
        return new UserLogoutResponse();
    }

    @NonNull
    @Override
    @SneakyThrows
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final UserProfileRequest request) {
        final Session session = check(request);
        final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}

