package ru.tsc.borisyuk.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.service.IProjectService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.dto.request.AbstractUserRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.system.AccessDeniedException;
import ru.tsc.borisyuk.tm.model.Session;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NonNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(
            final AbstractUserRequest request,
            final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        final String token = request.getToken();
        @NonNull final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;

    }

    protected Session check(final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        final String token = request.getToken();
        if (StringUtils.isEmpty(token)) throw new AccessDeniedException();
        @NonNull final Session session = serviceLocator.getAuthService().validateToken(token);
        return session;
    }

    @NonNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NonNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

}
