package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M add(String userId, M item);

    M remove(String userId, M item);

    void clear(String userId);

    M findOneByIndex(String userId, Integer index);

    M findOneById(String userId, String id);

    int size(String userId);

}
