package ru.tsc.borisyuk.tm.exception.entity;

public final class UserExistsByEmailException extends AbstractEntityExistsException {

    public UserExistsByEmailException() {
        super("Error! User email already exists...");
    }

}
