package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.*;


public interface IServiceLocator {

    @NonNull
    ICommandService getCommandService();

    @NonNull
    ILoggerService getLoggerService();

    @NonNull
    IPropertyService getPropertyService();

    @NonNull
    IAuthEndpoint getAuthEndpoint();

    @NonNull
    IProjectEndpoint getProjectEndpoint();

    @NonNull
    ITaskEndpoint getTaskEndpoint();

    @NonNull
    IUserEndpoint getUserEndpoint();

    @NonNull
    ISystemEndpoint getSystemEndpoint();

    @NonNull
    IDomainEndpoint getDomainEndpoint();

    @NonNull
    ITokenService getTokenService();

}