package ru.tsc.borisyuk.tm.api.client;

import ru.tsc.borisyuk.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {

}