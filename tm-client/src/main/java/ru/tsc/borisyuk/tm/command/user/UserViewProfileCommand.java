package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserProfileRequest;
import ru.tsc.borisyuk.tm.dto.response.UserProfileResponse;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.UserNotFoundException;
import ru.tsc.borisyuk.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-view-profile";

    @NonNull
    public static final String DESCRIPTION = "View user profile.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        @NonNull UserProfileRequest request = new UserProfileRequest();
        @NonNull final UserProfileResponse userProfileResponse = getServiceLocator().getAuthEndpoint().profile(request);
        final User user = userProfileResponse.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + Role.toName(user.getRole()));
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
