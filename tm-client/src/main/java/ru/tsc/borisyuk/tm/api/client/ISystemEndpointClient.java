package ru.tsc.borisyuk.tm.api.client;

import ru.tsc.borisyuk.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {

}