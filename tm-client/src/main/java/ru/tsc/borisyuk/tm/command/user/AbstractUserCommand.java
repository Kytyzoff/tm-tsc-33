package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;

import ru.tsc.borisyuk.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.borisyuk.tm.api.endpoint.IUserEndpoint;

import ru.tsc.borisyuk.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NonNull
    protected IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NonNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

}