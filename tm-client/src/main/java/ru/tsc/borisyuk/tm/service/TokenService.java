package ru.tsc.borisyuk.tm.service;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.borisyuk.tm.api.service.ITokenService;

@Getter
@Setter
public final class TokenService implements ITokenService {

    private String token;

}
