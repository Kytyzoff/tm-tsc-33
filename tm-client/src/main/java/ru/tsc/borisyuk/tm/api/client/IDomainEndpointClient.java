package ru.tsc.borisyuk.tm.api.client;

import ru.tsc.borisyuk.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {

}