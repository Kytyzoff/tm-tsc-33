package ru.tsc.borisyuk.tm.client;

import lombok.*;
import ru.tsc.borisyuk.tm.api.client.IEndpointClient;
import ru.tsc.borisyuk.tm.dto.response.ApplicationErrorResponse;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NonNull
    protected String host = "localhost";

    @NonNull
    protected Integer port = 6060;

    private Socket socket;

    public AbstractEndpointClient(@NonNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @SneakyThrows
    protected <T> T call(final Object data, Class<T> clazz) {
        if (data == null) return null;
        final ObjectOutputStream objectOutputStream = getObjectOutputStream();
        if (objectOutputStream == null) return null;
        objectOutputStream.writeObject(data);
        final ObjectInputStream objectInputStream = getObjectInputStream();
        if (objectInputStream == null) return null;
        final Object result = objectInputStream.readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NonNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        if (socket == null) return null;
        return new ObjectOutputStream(socket.getOutputStream());
    }

    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        if (socket == null) return null;
        return new ObjectInputStream(socket.getInputStream());
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        socket.close();
    }

}
