package ru.tsc.borisyuk.tm.command.system;

import lombok.NonNull;

public final class ExitCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "exit";

    @NonNull
    public static final String DESCRIPTION = "Close application Task Manager.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
