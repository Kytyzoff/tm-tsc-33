package ru.tsc.borisyuk.tm.client;

import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.borisyuk.tm.dto.request.ServerVersionRequest;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class SystemEndpointClientSoap {

    @SneakyThrows
    public static void main(String[] args) {
        final String wsdl = "http://127.0.0.1:8080/SystemEndpoint?wsdl";
        final ISystemEndpoint endpoint = Service.create(
                new URL(wsdl),
                new QName("http://endpoint.tm.borisyuk.tsc.ru/","SystemEndpointService")
        ).getPort(ISystemEndpoint.class);
        System.out.println(endpoint.getVersion(new ServerVersionRequest()).getVersion());;
        System.out.println(ISystemEndpoint.newInstance().getVersion(new ServerVersionRequest()).getVersion());
    }

}
