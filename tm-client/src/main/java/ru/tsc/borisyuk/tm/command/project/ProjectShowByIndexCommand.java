package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectGetByIndexRequest;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-show-by-index";

    @NonNull
    public static final String DESCRIPTION = "Show project by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NonNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest();
        request.setIndex(index);
        request.setToken(getToken());
        final Project project = getProjectEndpoint().getProjectByIndex(request).getProject();
        showProject(project);
    }

}
