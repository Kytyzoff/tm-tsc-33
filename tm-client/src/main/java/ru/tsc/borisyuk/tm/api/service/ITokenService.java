package ru.tsc.borisyuk.tm.api.service;

public interface ITokenService {

    String getToken();

    void setToken(String token);

}