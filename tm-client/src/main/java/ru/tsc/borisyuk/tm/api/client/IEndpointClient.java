package ru.tsc.borisyuk.tm.api.client;

import java.net.Socket;

public interface IEndpointClient {

    void connect();

    void disconnect();

    Socket getSocket();

    void setSocket(Socket socket);

}
