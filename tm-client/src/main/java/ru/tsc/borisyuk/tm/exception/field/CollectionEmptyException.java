package ru.tsc.borisyuk.tm.exception.field;

public final class CollectionEmptyException extends AbstractFieldException {

    public CollectionEmptyException() {
        super("Error! Collection is empty...");
    }

}
