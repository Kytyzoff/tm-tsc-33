package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.client.IDomainEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @NonNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NonNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NonNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NonNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataJsonFasterLoadResponse loadDataJsonFaster(@NonNull final DataJsonFasterLoadRequest request) {
        return call(request, DataJsonFasterLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NonNull final DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataXmlFasterLoadResponse loadDataXmlFaster(@NonNull final DataXmlFasterLoadRequest request) {
        return call(request, DataXmlFasterLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NonNull final DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataYamlFasterLoadResponse loadDataYamlFaster(@NonNull final DataYamlFasterLoadRequest request) {
        return call(request, DataYamlFasterLoadResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NonNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NonNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NonNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataJsonFasterSaveResponse saveDataJsonFaster(@NonNull final DataJsonFasterSaveRequest request) {
        return call(request, DataJsonFasterSaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NonNull final DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataXmlFasterSaveResponse saveDataXmlFaster(@NonNull final DataXmlFasterSaveRequest request) {
        return call(request, DataXmlFasterSaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NonNull final DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public DataYamlFasterSaveResponse saveDataYamlFaster(@NonNull final DataYamlFasterSaveRequest request) {
        return call(request, DataYamlFasterSaveResponse.class);
    }

}
