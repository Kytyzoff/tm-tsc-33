package ru.tsc.borisyuk.tm.exception.entity;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public abstract class AbstractEntityExistsException extends AbstractException {

    public AbstractEntityExistsException() {
    }

    public AbstractEntityExistsException(final String message) {
        super(message);
    }

    public AbstractEntityExistsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityExistsException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityExistsException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
