package ru.tsc.borisyuk.tm.enumerated;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.comparator.CreatedComparator;
import ru.tsc.borisyuk.tm.comparator.DateBeginComparator;
import ru.tsc.borisyuk.tm.comparator.NameComparator;
import ru.tsc.borisyuk.tm.comparator.StatusComparator;

import java.util.Arrays;
import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (StringUtils.isEmpty(value)) return null;
        return Arrays.stream(values())
                .filter(sort -> sort.name().equals(value))
                .findAny().orElse(null);
    }

}
