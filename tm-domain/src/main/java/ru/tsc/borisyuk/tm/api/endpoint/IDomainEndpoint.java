package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;


@WebService
public interface IDomainEndpoint extends IEndpoint {

    String NAME = "DomainEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NonNull final String host, @NonNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NonNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IDomainEndpoint.class);
    }

    @NonNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBackupLoadRequest request
    );

    @NonNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBase64LoadRequest request
    );

    @NonNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBinaryLoadRequest request
    );

    @NonNull
    @WebMethod
    DataJsonFasterLoadResponse loadDataJsonFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataJsonFasterLoadRequest request
    );

    @NonNull
    @WebMethod
    DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataJsonJaxbLoadRequest request
    );

    @NonNull
    @WebMethod
    DataXmlFasterLoadResponse loadDataXmlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataXmlFasterLoadRequest request
    );

    @NonNull
    @WebMethod
    DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataXmlJaxbLoadRequest request
    );

    @NonNull
    @WebMethod
    DataYamlFasterLoadResponse loadDataYamlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataYamlFasterLoadRequest request
    );

    @NonNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBackupSaveRequest request
    );

    @NonNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBase64SaveRequest request
    );

    @NonNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataBinarySaveRequest request
    );

    @NonNull
    @WebMethod
    DataJsonFasterSaveResponse saveDataJsonFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataJsonFasterSaveRequest request
    );

    @NonNull
    @WebMethod
    DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataJsonJaxbSaveRequest request
    );

    @NonNull
    @WebMethod
    DataXmlFasterSaveResponse saveDataXmlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataXmlFasterSaveRequest request
    );

    @NonNull
    @WebMethod
    DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataXmlJaxbSaveRequest request
    );

    @NonNull
    @WebMethod
    DataYamlFasterSaveResponse saveDataYamlFaster(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull DataYamlFasterSaveRequest request
    );

}
