package ru.tsc.borisyuk.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListResponse extends AbstractResponse {

    private List<Task> tasks;

    public TaskListResponse(final List<Task> tasks) {
        this.tasks = tasks;
    }

}
