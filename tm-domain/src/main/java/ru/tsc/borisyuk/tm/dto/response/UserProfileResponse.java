package ru.tsc.borisyuk.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.borisyuk.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(final User user) {
        super(user);
    }

}
