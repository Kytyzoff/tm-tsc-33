package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    private String token;

    public AbstractUserRequest(final String token) {
        this.token = token;
    }

}
