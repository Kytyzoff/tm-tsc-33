package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    String NAME = "UserEndpoint";

    String PART = NAME + "Service";


    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NonNull final String host, @NonNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NonNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IUserEndpoint.class);
    }


    @NonNull
    @WebMethod
    UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserLockRequest request
    );

    @NonNull
    @WebMethod
    UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserUnlockRequest request
    );

    @NonNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserChangePasswordRequest request
    );

    @NonNull
    @WebMethod
    UserRegisterResponse registerUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserRegisterRequest request
    );

    @NonNull
    @WebMethod
    UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserRemoveRequest request
    );

    @NonNull
    @WebMethod
    UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserUpdateProfileRequest request
    );

}