package ru.tsc.borisyuk.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    private String token;

    public UserLoginResponse(@NonNull final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(final String token) {
        this.token = token;
    }

}
