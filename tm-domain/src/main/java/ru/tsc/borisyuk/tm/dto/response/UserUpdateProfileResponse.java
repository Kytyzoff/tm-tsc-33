package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(final User user) {
        super(user);
    }

}