package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.borisyuk.tm.api.model.IWBS;
import ru.tsc.borisyuk.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS, Serializable {

    @NonNull
    private static final long serialVersionUID = 1;

    @NonNull
    private String name = "";

    @NonNull
    private String description = "";

    @NonNull
    private Status status = Status.NOT_STARTED;

    private String projectId;

    @NonNull
    private Date created = new Date();

    private Date dateBegin;

    private Date dateEnd;

    @Override
    public String toString() {
        return name + " : " + Status.toName(status) + " : " + description;
    }

}
