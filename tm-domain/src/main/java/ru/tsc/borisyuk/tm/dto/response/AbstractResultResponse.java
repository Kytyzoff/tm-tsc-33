package ru.tsc.borisyuk.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AbstractResultResponse extends AbstractResponse {

    @NonNull
    private Boolean success = true;

    private String message = "";

    public AbstractResultResponse(@NonNull final Throwable throwable) {
        setSuccess(false);
        setMessage(throwable.getMessage());
    }

}
