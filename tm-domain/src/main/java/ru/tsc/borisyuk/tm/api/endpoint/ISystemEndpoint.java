package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.dto.request.ServerAboutRequest;
import ru.tsc.borisyuk.tm.dto.request.ServerVersionRequest;
import ru.tsc.borisyuk.tm.dto.response.ServerAboutResponse;
import ru.tsc.borisyuk.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    String NAME = "SystemEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NonNull final String host, @NonNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, ISystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NonNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, ISystemEndpoint.class);
    }

    @NonNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull ServerAboutRequest request
    );

    @NonNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull ServerVersionRequest request
    );

}

