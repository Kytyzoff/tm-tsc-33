package ru.tsc.borisyuk.tm.exception.system;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public final class LogDirectoryCreateException extends AbstractException {

    public LogDirectoryCreateException(final String message) {
        super(String.format("Error! Can`t create log directory ``%s``", message));
    }

}
