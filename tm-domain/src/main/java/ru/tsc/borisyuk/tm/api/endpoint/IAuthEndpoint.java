package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLogoutRequest;
import ru.tsc.borisyuk.tm.dto.request.UserProfileRequest;
import ru.tsc.borisyuk.tm.dto.response.UserLoginResponse;
import ru.tsc.borisyuk.tm.dto.response.UserLogoutResponse;
import ru.tsc.borisyuk.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    String NAME = "AuthEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NonNull final String host, @NonNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NonNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IAuthEndpoint.class);
    }


    @NonNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserLoginRequest request
    );

    @NonNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserLogoutRequest request
    );

    @NonNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull UserProfileRequest request
    );


}
