package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResponse {

}
