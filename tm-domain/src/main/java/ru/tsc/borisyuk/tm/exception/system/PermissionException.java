package ru.tsc.borisyuk.tm.exception.system;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Insufficient rights to execute command...");
    }

}
