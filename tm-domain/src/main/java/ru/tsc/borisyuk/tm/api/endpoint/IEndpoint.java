package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public interface IEndpoint {

    String REQUEST = "request";

    String HOST = "127.0.0.1";

    String PORT = "8080";

    String NAMESPACE = "http://endpoint.tm.borisyuk.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NonNull final IConnectionProvider connectionProvider,
            @NonNull final String name,
            @NonNull final String namespace,
            @NonNull final String part,
            @NonNull final Class<T> clazz
    ) {
        @NonNull final String host = connectionProvider.getHost();
        @NonNull final String port = connectionProvider.getPort();
        return newInstance(host, port, name, namespace, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NonNull final String host,
            @NonNull final String port,
            @NonNull final String name,
            @NonNull final String namespace,
            @NonNull final String part,
            @NonNull final Class<T> clazz
    ) {
        @NonNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NonNull final URL url = new URL(wsdl);
        @NonNull final QName qName = new QName(namespace, part);
        return Service.create(url, qName).getPort(clazz);
    }

}
